import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  projectName = 'Php Project';

  constructor() {
    setTimeout (() => {
      this.projectName = 'Java Project';
    }, 3000);
  }

  ngOnInit() {
  }

  

}
