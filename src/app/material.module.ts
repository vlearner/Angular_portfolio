import { NgModule } from '@angular/core';
import {MatToolbarModule, MatToolbar, MatToolbarRow} from '@angular/material/toolbar';


@NgModule({
    imports: [MatToolbarModule],

    exports: [MatToolbarModule]
})

export class MaterialModule { }
